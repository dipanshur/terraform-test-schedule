module "lncloud10100001" {
    source = "./modules/iaac"
    
    identifier = "lncloud10100001"
    vpc_cidr = "10.0.0.0/16"
    public_subnet_cidr = "10.1.0.0/24"
    private_subnet_cidr = "10.2.0.0/24"
    availability_zone_public = "us-east-1a"
    availability_zone_private = "us-east-1a"
}

module "lncloud10100002" {
    source = "./modules/iaac"
    
    identifier = "lncloud10100002"
    vpc_cidr = "192.168.0.0/16"
    public_subnet_cidr = "192.168.1.0/24"
    private_subnet_cidr = "192.168.2.0/24"
    availability_zone_public = "us-east-1a"
    availability_zone_private = "us-east-1a"
}


