terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "tsi-terraform-state-bucket-1"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-state-locking-table"
    profile        = "default"
  }
}
