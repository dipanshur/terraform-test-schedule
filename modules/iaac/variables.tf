variable "identifier" {
    type = string
}

/*VPC Vars */
variable "vpc_cidr" {
    type = string
    default = "10.1.0.0/26"

}

variable "public_subnet_cidr" {
    type = string
    default = "10.1.0.0/28"
}

variable "private_subnet_cidr" {
    type = string
    default = "10.1.0.0/28"
}

variable "availability_zone_public" {
    type = string
    default = ""

}

variable "availability_zone_private" {
    type = string
    default = ""
}

