resource "aws_vpc" "default" {
    cidr_block = var.vpc_cidr
    tags = {
        Name = "Default VPC"
    }
}

resource "aws_subnet" "public_subnet" {
    vpc_id = aws_vpc.default.id
    cidr_block = var.public_subnet_cidr
    availability_zone = var.availability_zone_public
    tags = {
        Name = "Web Public Subnet - $(var.identifier)"
    }
}

resource "aws_subnet" "private_subnet" {
    vpc_id = aws_vpc.default.id
    cidr_block = var.private_subnet_cidr
    availability_zone = var.availability_zone_private
    tags = {
        Name = "Web Private Subnet - $(var.identifier)"
    }
}

resource "aws_internet_gateway" "igw" {
    vpc_id = aws_vpc.default.id

    tags = {
        Name = "aws_internet_gateway - $(vars.identifier)"
    }
}

resource "aws_route_table" "public-RT" {
    vpc_id = aws_vpc.default.id

    tags = {
        Name = "Public-RT-$(vars.identifier)"
    }
}

resource "aws_route_table" "private-RT" {
    vpc_id = aws_vpc.default.id

    tags = {
        Name = "Private-RT-$(vars.identifier)"
    }

}

#### Add IGW to Public RT

resource "aws_route" "igw_route" {
    route_table_id = aws_route_table.public-RT.id
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
    depends_on = [aws_internet_gateway.igw]
}

## Assign RT to Subnet

resource "aws_route_table_association" "public-RT-subnet" {
    subnet_id = aws_subnet.public_subnet.id
    route_table_id = aws_route_table.public-RT.id
}
resource "aws_route_table_association" "private-RT-subnet" {
    subnet_id = aws_subnet.private_subnet.id
    route_table_id = aws_route_table.private-RT.id
}

